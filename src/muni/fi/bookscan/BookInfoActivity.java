package muni.fi.bookscan;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

/** Activity creating new view with information about book clicked. */
public class BookInfoActivity extends Activity{
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //Pulling book info from "extra"
        Intent intent = getIntent();
        ArrayList<String> bookInfo = intent.getStringArrayListExtra("bookInfo");
        
        setContentView(R.layout.item_view);
        
        
        TextView text;
        
        //Putting info to each row of table layout
        text = (TextView) findViewById(R.id.text1);
        text.setText(Html.fromHtml("<b>"+getString(R.string.title)+"</b><br />"+bookInfo.get(0)));
        
        text = (TextView) findViewById(R.id.text2);
        text.setText(Html.fromHtml("<b>"+getString(R.string.author)+"</b><br />"+bookInfo.get(1)));
        
        text = (TextView) findViewById(R.id.text3);
        text.setText(Html.fromHtml("<b>"+getString(R.string.publisher)+"</b><br />"+bookInfo.get(2)));
        
        text = (TextView) findViewById(R.id.text4);
        text.setText(Html.fromHtml("<b>"+getString(R.string.published_date)+"</b><br />"+bookInfo.get(3)));
        
        text = (TextView) findViewById(R.id.text5);
        text.setText(Html.fromHtml("<b>"+getString(R.string.page_count)+"</b><br />"+bookInfo.get(5)));
        
        text = (TextView) findViewById(R.id.text6);
        text.setText(Html.fromHtml("<b>"+getString(R.string.categories)+"</b><br />"+bookInfo.get(6)));
        
        text = (TextView) findViewById(R.id.text7);
        text.setText(Html.fromHtml("<b>"+getString(R.string.description)+"</b><br />"+bookInfo.get(4)));
        
        text = (TextView) findViewById(R.id.text8);
        text.setText(Html.fromHtml("<b>"+getString(R.string.language)+"</b><br />"+bookInfo.get(9)));
        
        text = (TextView) findViewById(R.id.text9);
        text.setText(Html.fromHtml("<b>"+getString(R.string.average_rating)+":</b> "+bookInfo.get(7)
        		+" ("+bookInfo.get(8)+" "+getString(R.string.ratings_count)+")"));
        
    }
}
